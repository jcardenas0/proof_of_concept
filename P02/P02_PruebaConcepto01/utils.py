import numpy as np
import pandas as pd
import csv
import matplotlib.pyplot as plt
from sklearn import datasets

import os

from pandas import DataFrame, HDFStore
from pandas import ExcelWriter
from pandas import ExcelFile
from slugify import slugify, Slugify, UniqueSlugify
import seaborn as sns
import warnings
import tables

os.getcwd()

def write_unsafe_attrs() :
    original_warnings = list(warnings.filters)
    warnings.simplefilter('ignore', tables.NaturalNameWarning)


def read_CSV():
    
    global df,df_train, df_test, num_columns, num_rows, num_rows_train

    df = pd.read_csv("2018TestData.csv") 
    df = format_column_names(df)

    num_columns=len(df.columns)
    num_rows=len(df.index)

    df_train = df.sample(frac=0.8,random_state=0)
    df_test = df.drop(df_train.index)
        
    return [df_train, df_test];


def close_dataset():
    store.close()
    

def format_column_names(data_to_format):

    new_array = []

    for i, column in enumerate(data_to_format.columns):
        
        
        custom_slugify = Slugify(to_lower=True)
        custom_slugify.separator = '_'
        string = custom_slugify(column)          # 'any_text'
        new_array.append(string)
    
    data_to_format.columns = new_array
    print(new_array)

    return data_to_format
    
def create_dataStructure():

    global store
    store = HDFStore('dataset.h5')
    


def convert_excel_to_hdf():

    #data = DataFrame(np.random.randn(num_rows,num_columns), columns=df.columns)
    data_train = DataFrame(df_train, columns=df.columns)
    data_test = DataFrame(df_test, columns=df.columns)

    #sns.pairplot(data_train[["und_ped","pedidos_real"]], diag_kind="kde")

    #print(df.dtypes)

    store.append('data_train', data_train, data_columns=True,min_itemsize=20)
    store.append('data_test', data_test, data_columns=True,min_itemsize=20)
    
    
