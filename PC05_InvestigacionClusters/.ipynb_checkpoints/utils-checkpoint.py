import numpy as np
import pandas as pd
import csv
import matplotlib.pyplot as plt
from sklearn import datasets

import os

from pandas import DataFrame, HDFStore
from pandas import ExcelWriter
from pandas import ExcelFile
from slugify import slugify, Slugify, UniqueSlugify
import seaborn as sns
import warnings
import tables
import xlrd


os.getcwd()



def write_unsafe_attrs() :
    original_warnings = list(warnings.filters)
    warnings.simplefilter('ignore', tables.NaturalNameWarning)

def read_Excel():
    global dfLabels
    excel = xlrd.open_workbook('ScopusData.xlsx')
    sheet_0 = excel.sheet_by_index(0) # Open the first tab
    sheet_1 = excel.sheet_by_index(1) # Open the first tab

    return [sheet_0,sheet_1]

def read_CSV(name):
    
    global df,df_train, df_test, num_columns, num_rows, num_rows_train

    df = pd.read_csv(name) 
    df = format_column_names(df)

    num_columns=len(df.columns)
    num_rows=len(df.index)

    
    return [df];


def close_dataset():
    store.close()
    

def format_column_names(data_to_format):

    new_array = []

    for i, column in enumerate(data_to_format.columns):
        
        
        custom_slugify = Slugify(to_lower=True)
        custom_slugify.separator = '_'
        string = custom_slugify(column)          # 'any_text'
        new_array.append(string)
    
    data_to_format.columns = new_array
    print(new_array)

    return data_to_format
    
def create_dataStructure(name):

    global store
    store = HDFStore(name, mode= 'a')
    return store


def convert_excel_to_hdf():

    #data = DataFrame(np.random.randn(num_rows,num_columns), columns=df.columns)
    data_train = DataFrame(df_train, columns=df.columns)
    data_test = DataFrame(df_test, columns=df.columns)

    #sns.pairplot(data_train[["und_ped","pedidos_real"]], diag_kind="kde")

    #print(df.dtypes)

    store.append('data_train', data_train, data_columns=True,min_itemsize=20)
    store.append('data_test', data_test, data_columns=True,min_itemsize=20)
    
def convert_to_hdf(dataSet):

    #data = DataFrame(np.random.randn(num_rows,num_columns), columns=df.columns)

    store.append('data_train', dataSet, data_columns=True,min_itemsize=50)

def convert_to_hdf_name(dataSet,name):

    #data = DataFrame(np.random.randn(num_rows,num_columns), columns=df.columns)

    store.append(name, dataSet, data_columns=True,min_itemsize=50)
    
def CombineColumns(arrayName,dataSet):

    for i in enumerate(dataSet[arrayName[0]]): 
        j = dataSet[arrayName[1]][i[0]] 
        # printing the third element of the column 
        if i[1]!='' and j!='':
            dataSet[arrayName[0]].iloc[i[0]] =  i[1]+ "/" +j
        else:
            dataSet[arrayName[0]].iloc[i[0]] =  i[1]+j
    
    
    return dataSet

def ReplaceValuesColumns(arrayNames,dataSet):
    keyArray=[]
    for i in arrayNames: 
        keyArray.append(i)
        dataSet[i] = dataSet[i].replace('1', arrayNames[i])
    
      
    return dataSet

def addColValues(arrayNames,dataSet):
    keyArray=[]
    string=''
    for i in arrayNames: 
        keyArray.append(i)

    for j in range(len(keyArray)-1, 0 , -1):
        dataSet[keyArray[0]] =  dataSet[keyArray[0]] + dataSet[keyArray[j]]   
   
    return dataSet

def dropCols(arrayNames,dataSet):
    keyArray=[]
    for i in arrayNames: 
        keyArray.append(i)

    for j in keyArray:
        dataSet.drop(j, axis=1, inplace=True)   
   
    return dataSet


def createColsfromCellValues(dataset, colname):
    custom_slugify = Slugify(to_lower=True)
    custom_slugify.separator = '_'

    uniquevalues = list(filter(lambda x : x != '', dataset[colname].unique()))  
    uniquevaluesCleaned =[]
    for loc in uniquevalues:
        loc = str(loc)   #here 'nan' is converted to a string to compare with if
        if loc != 'nan' and loc != '1' :
            uniquevaluesCleaned.append(loc)

    for item in uniquevaluesCleaned:
        if type(item) != np.nan:
            dataset[custom_slugify(item.replace('\r', ''))] = dataset[colname].apply(lambda x: np.all(x==item))