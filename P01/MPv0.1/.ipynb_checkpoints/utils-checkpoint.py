import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os

from pandas import DataFrame, HDFStore
from pandas import ExcelWriter
from pandas import ExcelFile
from slugify import slugify
import seaborn as sns

from sklearn.preprocessing import LabelEncoder

def create_dataStructure():

    global store
    store = HDFStore('dataset.h5')


def read_excel():
    global df,df_train, df_test, num_columns, num_rows, num_rows_train

    df = pd.read_excel('Libro1.xlsx', sheet_name='Brasier')

    df = format_column_names(df)
    #df = pd.get_dummies(df, columns=["clasificacion_ll"]).head()
    df = data_encoding(df)

    num_columns=len(df.columns)
    num_rows=len(df.index)

    df_train = df.sample(frac=0.8,random_state=0)
    df_test = df.drop(df_train.index)

    #num_rows_train = calc_data_rows()
   # df_train = df.iloc[0:num_rows_train]
   # df_test = df.iloc[num_rows_train+1:num_rows-1]


    return df.columns

def read_excel_predict():
    df_prediction = pd.read_excel('Libro2.xlsx', sheet_name='Brasier')
    df_prediction = format_column_names(df_prediction)
    df_prediction = data_encoding(df_prediction)

    return df_prediction


def close_dataset():
    store.close()


def convert_excel_to_hdf():

    #data = DataFrame(np.random.randn(num_rows,num_columns), columns=df.columns)
    data_train = DataFrame(df_train, columns=df.columns)
    data_test = DataFrame(df_test, columns=df.columns)

    sns.pairplot(data_train[["und_ped","pedidos_real"]], diag_kind="kde")

    print(df.dtypes)

    store.append('data_train', data_train, data_columns=True,min_itemsize=20)
    store.append('data_test', data_test, data_columns=True,min_itemsize=20)

def format_column_names(data_to_format):

    new_array = []
    for i, column in enumerate(data_to_format.columns):
        new_array.append(slugify(column).replace("-", "_"))

    data_to_format.columns = new_array

    return data_to_format

def calc_data_rows():
    train_rows = int(round(num_rows * 0.8))
    return train_rows


def random_rows():

    return df.reindex(np.random.permutation(df.index))

def data_encoding(data_to_format):

    lb_make = LabelEncoder()

    data_to_format.replace('', np.nan, inplace=True)

    data_to_format['pagina'].fillna(0, inplace=True) #paginas vacias se convierte en pagina 0
    data_to_format['color'].fillna('NINGUNO', inplace=True) #paginas vacias se convierte en pagina 0
    data_to_format['color_mp'].fillna('NINGUNO', inplace=True) #paginas vacias se convierte en pagina 0
    data_to_format['uen'].fillna('NINGUNO', inplace=True) #paginas vacias se convierte en pagina 0
    data_to_format['segmenta_textil'].fillna('NINGUNO', inplace=True)
    data_to_format['segmenta_edad'].fillna('NINGUNO', inplace=True)
    data_to_format['segmenta_b_i_m'].fillna('NINGUNO', inplace=True)
    data_to_format['es_nvo'].fillna('NINGUNO', inplace=True)
    data_to_format['genero'].fillna('NINGUNO', inplace=True)
    data_to_format['linea'].fillna('NINGUNO', inplace=True)
    data_to_format['estrateg_exhib'].fillna('NINGUNO', inplace=True)
    data_to_format['categoria'].fillna('NINGUNO', inplace=True)
    data_to_format['clase_prenda'].fillna('NINGUNO', inplace=True)
    data_to_format['silueta'].fillna('NINGUNO', inplace=True)
    data_to_format['estilo'].fillna('NINGUNO', inplace=True)
    data_to_format['copa'].fillna('NINGUNO', inplace=True)
    data_to_format['tipo_copa'].fillna('NINGUNO', inplace=True)
    data_to_format['aro'].fillna('NINGUNO', inplace=True)
    data_to_format['realce'].fillna('NINGUNO', inplace=True)
    data_to_format['beneficios'].fillna('NINGUNO', inplace=True)
    data_to_format['conjunto_sup'].fillna('NINGUNO', inplace=True)
    data_to_format['espalda'].fillna('NINGUNO', inplace=True)
    data_to_format['conjunto_inferior'].fillna('NINGUNO', inplace=True)
    data_to_format['material'].fillna('NINGUNO', inplace=True)
    data_to_format['estampado_superior'].fillna('NINGUNO', inplace=True)
    data_to_format['estampado_inferior'].fillna('NINGUNO', inplace=True)


    #=========== Crear encoding.

    data_to_format["color_code"] = lb_make.fit_transform(data_to_format["color"])
    data_to_format["color_mp_code"] = lb_make.fit_transform(data_to_format["color_mp"])
    data_to_format["clasificacion_ll_code"] = lb_make.fit_transform(data_to_format["clasificacion_ll"])
    data_to_format["descripcion_code"] = lb_make.fit_transform(data_to_format["descripcion"])
    data_to_format["talla_code"] = lb_make.fit_transform(data_to_format["talla"])
    data_to_format["vehiculo_code"] = lb_make.fit_transform(data_to_format["vehiculo"])
    data_to_format["rango_precio_code"] = lb_make.fit_transform(data_to_format["rango_precio"].astype(str))
    data_to_format["uen_code"] = lb_make.fit_transform(data_to_format["uen"])
    data_to_format["segmenta_textil_code"] = lb_make.fit_transform(data_to_format["segmenta_textil"])
    data_to_format["segmenta_edad_code"] = lb_make.fit_transform(data_to_format["segmenta_edad"])
    data_to_format["segmenta_b_i_m_code"] = lb_make.fit_transform(data_to_format["segmenta_b_i_m"])
    data_to_format["es_nvo_code"] = lb_make.fit_transform(data_to_format["es_nvo"])
    data_to_format["genero_code"] = lb_make.fit_transform(data_to_format["genero"])
    data_to_format["linea_code"] = lb_make.fit_transform(data_to_format["linea"])
    data_to_format["estrateg_exhib_code"] = lb_make.fit_transform(data_to_format["estrateg_exhib"])
    data_to_format["categoria_code"] = lb_make.fit_transform(data_to_format["categoria"])
    data_to_format["clase_prenda_code"] = lb_make.fit_transform(data_to_format["clase_prenda"])
    data_to_format["silueta_code"] = lb_make.fit_transform(data_to_format["silueta"])
    data_to_format["estilo_code"] = lb_make.fit_transform(data_to_format["estilo"])
    data_to_format["copa_code"] = lb_make.fit_transform(data_to_format["copa"])
    data_to_format["tipo_copa_code"] = lb_make.fit_transform(data_to_format["tipo_copa"])
    data_to_format["aro_code"] = lb_make.fit_transform(data_to_format["aro"])
    data_to_format["realce_code"] = lb_make.fit_transform(data_to_format["realce"])
    data_to_format["beneficios_code"] = lb_make.fit_transform(data_to_format["beneficios"])
    data_to_format["conjunto_sup_code"] = lb_make.fit_transform(data_to_format["conjunto_sup"])
    data_to_format["espalda_code"] = lb_make.fit_transform(data_to_format["espalda"])
    data_to_format["conjunto_inferior_code"] = lb_make.fit_transform(data_to_format["conjunto_inferior"])
    data_to_format["material_code"] = lb_make.fit_transform(data_to_format["material"])
    data_to_format["estampado_superior_code"] = lb_make.fit_transform(data_to_format["estampado_superior"])
    data_to_format["estampado_inferior_code"] = lb_make.fit_transform(data_to_format["estampado_inferior"])
    #=========== solo se hace conversión de tipo de datos.
    data_to_format["pagina"] = data_to_format["pagina"].astype(str).astype(float).astype(int)

    return data_to_format
