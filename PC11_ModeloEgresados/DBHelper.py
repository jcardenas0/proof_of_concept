import pypyodbc as podbc
import datetime
import numpy as np

datetime_object = datetime.datetime.now()


class DbClient:
    def __init__(self, db_name, server, user, password):
        self._db_name = db_name
        self._server = server
        self._user = user
        self._pwd = password
        
    def connect(self):
        string = "Driver={SQL Server};Server="+self._server+";Database="+self._db_name+";uid="+self._user+";pwd="+self._pwd
        print(string)
        self.connection = podbc.connect(string)
        self.cursor = self.connection.cursor()
    def close(self):
        self.cursor.close()
        self.connection.close()
        
    def fetchQuestions(self):
        sql = "SELECT * FROM ResearchBeta.dbo.questions"
        self.cursor.execute(sql)
        data = self.cursor.fetchone()
        dataArray = []
        while data: 
            dataArray.append(data)
            data = self.cursor.fetchone()

        self.connection.close()

        return dataArray
    
    def insertAnswer(self,arg1, arg2, personID):
        #args trae el array con datos del dataset
        #kwargs traer el diccionario con las referencias de la tabla que se quiere insertar
    
        row = arg1
        dictionaryAnswers = arg2
        
        # INSERT INTO [dbo].[answers]
        #    ([person_id]
       #    ,[question_id]
       #    ,[answer]
       #    ,[created_at]
       #    ,[updated_at])
         #VALUES
          # (1
          # ,5
          # ,'NS/NR'
          # ,GETDATE()
          # ,GETDATE())
        
        dataRow = []

        for key, value in dictionaryAnswers.items():
            dataRow = [
                personID,
                value,
                'NULL' if str(row[key])== 'nan' else str(row[key]),
                datetime_object,
                datetime_object
            ]
        
            sql = """
            INSERT INTO [dbo].[answers]
               ([person_id]
               ,[question_id]
               ,[answer]
               ,[created_at]
               ,[updated_at])
            VALUES (?, ?, ?, ? ,?)
            """
            self.cursor.execute(sql, dataRow)  
            dataRow = []
            
            self.connection.commit()
            
        row=[]
            
    def insertPerson(self,arg1, arg2):
        #args trae el array con datos del dataset
        #kwargs traer el diccionario con las referencias de la tabla que se quiere insertar
        
        row = arg1
        dictionaryPerson = arg2
        
        
        datarow=['NULL' if str(row['nombres_y_apellidos'])== 'nan' else str(row['nombres_y_apellidos']),
                'NULL',
                'C.C.',
                'NULL' if str(row['e_mail'])== 'nan' else str(row['e_mail']),
                'NULL' if str(row['p1_sex'])== 'nan' else str(row['p1_sex']),
                'NULL' if str(row['p4_estado_civil'])== 'nan' else str(row['p4_estado_civil']) ,
                'Medellin',
                'Colombia',
                 'NULL' if str(row['p2_edad'])== 'nan' else str(row['p2_edad']) ,
                'NULL' if str(row['direccion'])== 'nan' else str(row['direccion']),
                'NULL' if str(row['tel'])== 'nan' else str(row['tel']),
                '1',
                datetime_object,
                datetime_object,
                 'NULL' if str(row['p3_estrato_socioecon'])== 'nan' else str(row['p3_estrato_socioecon'])]
        
        sql = """
        INSERT INTO [dbo].[people]
           ([first_name]
           ,[last_name]
           ,[doc_type]
           ,[email]
           ,[gender]
           ,[civil_status]
           ,[city]
           ,[country]
           ,[age]
           ,[addr]
           ,[phone]
           ,[privacy_acceptance]
           ,[created_at]
           ,[updated_at]
           ,estrato) 
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
        """

        self.cursor.execute(sql, datarow)
        self.connection.commit()
        
        return (int(self.cursor.execute("SELECT @@IDENTITY as id").fetchone()[0]))